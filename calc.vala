using Gtk;

[GtkTemplate (ui = "/org/gavr/calc/calc.ui")]
public class MyWidget : Window {

	[GtkChild]
	public MyWidget () {
		this.show_all();
		this.destroy.connect (Gtk.main_quit);
	}

	[GtkCallback]
	private void button_clicked (Button button) {
		print ("The button was clicked with entry text");
	}

}

void main(string[] args) {
	Gtk.init (ref args);
	//var win = new Window();
	//win.destroy.connect (Gtk.main_quit);

	var widget = new MyWidget ();

	//win.add (widget);
	//win.show_all ();

	Gtk.main ();
}
